//card.h
//by Jack Wenger
//card object for blackjack
#ifndef CARD_H
#define CARD_H
#include <string>
class card{
public:
  card();
  card(int v,int s,std::string n);
  ~card();
  int suit();
  int value();
  std::string name();


private:
  int st;
  int val;
  std::string txt;
};
#endif
